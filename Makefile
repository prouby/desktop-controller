CC = gcc
CFLAGS = -W -Wall
LDFLAGS = -lSDL2 -lX11 -lXtst

all: cmouse

cmouse: cmouse.o
	@echo "  LD    $@"
	@$(CC) $(CFLAGS) cmouse.o -o cmouse $(LDFLAGS)

%.o: %.c
	@echo "  CC    $@"
	@$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	@echo "  RM"
	@rm -rvf cmouse
	@rm -rvf *.o
