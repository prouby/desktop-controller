/* cmouse.c -- Mouse control with game controller.
 *
 * Copyright (C) 2019 by Pierre-Antoine Rouby <contact@parouby.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include <SDL2/SDL.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/XTest.h>

#define SPEED 2
#define WAIT_TIME 16000
#define DEAD_ZONE 5000

#define LEFT_CLICK 1
#define RIGHT_CLICK 3

#define abs(x) ((x>0)?x:(-x))

void
raise_mouse_event (Display * display, int button, bool up)
{
  XTestFakeButtonEvent(display, button,
                       up ? False: True,
                       CurrentTime);
  XFlush(display);
}

void
get_mouse_pos (int * p_x, int * p_y,
               Display * d, Window root)
{
  Window in_win;
  Window in_child_win;
  int childx, childy;
  unsigned int mask;

  XQueryPointer(d, root, &in_win, &in_child_win,
                p_x, p_y, &childx, &childy, &mask);
}

void
rec_loop (Display * display, Window root,
          SDL_GameController * controller)
{
  int x, y;
  int cx, cy;
  SDL_Event event;

  while (SDL_PollEvent (&event))
    {
      if (event.type == SDL_QUIT)
          return;

      if (event.type == SDL_CONTROLLERBUTTONUP ||
          event.type == SDL_CONTROLLERBUTTONDOWN)
        {
          if (event.cbutton.button == SDL_CONTROLLER_BUTTON_A)
            raise_mouse_event (display,
                               LEFT_CLICK,
                               event.type == SDL_CONTROLLERBUTTONUP);

          if (event.cbutton.button == SDL_CONTROLLER_BUTTON_B)
            raise_mouse_event (display,
                               RIGHT_CLICK,
                               event.type == SDL_CONTROLLERBUTTONUP);
        }
    }

  get_mouse_pos (&x, &y, display, root);

  /* Get axis pos */
  cx = SDL_GameControllerGetAxis (controller,
                                  SDL_CONTROLLER_AXIS_LEFTX);
  cy = SDL_GameControllerGetAxis (controller,
                                  SDL_CONTROLLER_AXIS_LEFTY);

  /* Dead zone */
  if (abs(cx) > DEAD_ZONE) x += (cx * SPEED) / 1000;
  if (abs(cy) > DEAD_ZONE) y += (cy * SPEED) / 1000;

  if (abs(cx) > DEAD_ZONE || abs(cy) > DEAD_ZONE)
    {
      XWarpPointer(display, None, root, 0, 0, 0, 0, x, y);
      XFlush(display);
    }

  usleep (WAIT_TIME);

  rec_loop (display, root, controller);
}

SDL_GameController *
open_controller_rec (int i)
{
  SDL_GameController * controller;

  if (i > SDL_NumJoysticks()) {
    printf ("No Controller found\n");
    return NULL;
  }

  if (SDL_IsGameController(i))
    {
      controller = SDL_GameControllerOpen(i);
      if (controller)
          return controller;
    }

  return open_controller_rec (++i);
}

int
main (/* int argc, char **argv */)
{
  SDL_GameController * controller;
  Display * display;
  Window root;

  /* SDL init */
  if (SDL_Init (SDL_INIT_GAMECONTROLLER))
    {
      fprintf (stderr, "SDL Init: %s\n", SDL_GetError());
      exit (EXIT_FAILURE);
    }

  /* X11 init */
  display = XOpenDisplay(0);
  root = XRootWindow(display, 0);
  XSelectInput(display, root, KeyReleaseMask);

  /* Controller */
  controller = open_controller_rec (0);

  /* Main loop */
  if (controller != NULL)
    rec_loop (display, root, controller);

  /* Free */
  XCloseDisplay(display);
  SDL_Quit ();

  return 0;
}

/* cmouse.c -- End of file */
