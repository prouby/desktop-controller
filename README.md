# Desktop controller

Use pointer with game controller.

*BTW: It's just a small hack...*

## How to

| Mouse event  | Controller button |
|--------------|-------------------|
| Move pointer | Left axis         |
| Left click   | Button A          |
| Right click  | Button B          |

## Build and run

You need *sdl2*, *libx* and *libxtst*.

```shell
$ make
$ ./cmouse
```
